<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Sadasbor Portal - Sedang Dalam Perbaikan</title>
	<link href="https://fonts.googleapis.com/css?family=Outfit:300,400,400i,700,800,900&display=fallback">
	<link type="text/css" rel="stylesheet" href="dist/css/style.css" />
	<link rel="icon" href="docs/assets/img/sadasbor.png">
</head>

<body>

	<div id="notfound">
		<div class="notfound">
			<div class="notfound-404">
				<h1><span></span></h1>
			</div>
			<br>
			<h2>OOPS!</h2>
			<h2>HALAMAN SEDANG DALAM PERBAIKAN</h2>
			<p>Halaman WBS dan SKM sedang dalam proses migrasi. Halaman akan aktif dalam beberapa waktu kedepan.</p>
			<br>
			<button class="backbutton" onclick="window.location.href='index.html'">
				<span>Kembali Ke Portal</span>
			  </button>
			  
		</div>
	</div>
</body>

</html>
