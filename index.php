<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Sadasbor Portal</title>

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Outfit:300,400,400i,700,800,900&display=fallback">
  <link rel="stylesheet" href="dist/css/style.css">
  <link rel="icon" href="docs/assets/img/sadasbor.png">
</head>

<body>
    <div class="top-right-image">
        <img src="docs/assets/img/berakhlak.png" width="80">
    </div>
        <div class="logo-header">
            <center>
                <div>
                    <img src="docs/assets/img/sadasbor-shdw.png" width="160">
                </div>
                <div class="header-text">       
                    <p class="headertext-sadasbor">SADASBOR</p>
                    <p class="headertext-kabtasik">Kabupaten Tasikmalaya</p>
                </div> 
            </center>
        </div>
        
    <div class="container"> 
        <div class="contentkinerja">
            <img src="docs/assets/img/kinerja.svg" width="160">
            <button class="kinerja">KINERJA</button>
        </div>

        <div class="contenttalenta">
            <img src="docs/assets/img/talenta.svg" width="160" style="margin-top: 71.5px;">
            <a href="https://talenta.tasikmalayakab.go.id"><button class="talenta">TALENTA</button></a>
        </div>

        <div class="contentwbs">
            <img src="docs/assets/img/wbs.svg" width="160">
            <a href="error-page.html"><button class="wbs">WBS</button></a>
        </div>      

        <div class="contentsakip">
            <img src="docs/assets/img/sakip.svg" width="160">
            <button class="sakip">SAKIP</button>
        </div>

        <div class="contentskm">
            <img src="docs/assets/img/skm.svg" width="160">
            <a href="error-page.html"><button class="skm">SKM</button></a>
        </div>

        <div class="contentvms">
            <img src="docs/assets/img/vms.svg" width="160">
            <button class="vms">VMS</button>
        </div>
        
    
    <br>
        <button class="logoutbutton"><span>LOGOUT</span></button>
</div>
</body>

<footer class="footer-text">
    <img src="docs/assets/img/sadasbor2.png" width="80" style="margin-top: 120px;">
    <p class="footertext">Pemerintah Kabupaten Tasikmalaya ©2023</p>
</footer>

</body>
</html>
